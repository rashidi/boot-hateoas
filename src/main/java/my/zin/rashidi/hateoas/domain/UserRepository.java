package my.zin.rashidi.hateoas.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Rashidi Zin.
 */
public interface UserRepository extends MongoRepository<User, String> {
}
