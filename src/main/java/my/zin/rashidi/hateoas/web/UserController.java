package my.zin.rashidi.hateoas.web;

import my.zin.rashidi.hateoas.domain.User;
import my.zin.rashidi.hateoas.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Rashidi Zin.
 */
@RestController
@RequestMapping("users")
@ExposesResourceFor(User.class)
public class UserController {

    private final UserRepository repository;

    private final EntityLinks entityLinks;

    @Autowired
    public UserController(UserRepository repository, EntityLinks entityLinks) {
        this.repository = repository;
        this.entityLinks = entityLinks;
    }

    @RequestMapping(method = RequestMethod.POST)
    HttpEntity<Resource<User>> save(@RequestBody User user) {
        user = repository.save(user);
        Resource<User> resource = new Resource<>(user);
        resource.add(entityLinks.linkToSingleResource(User.class, user.getId()));

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    HttpEntity<Resources<User>> findAll() {
        Resources<User> resources = new Resources<>(repository.findAll());
        resources.add(entityLinks.linkToCollectionResource(User.class));

        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "{id}")
    HttpEntity<Resource<User>> get(@PathVariable("id") String id) {
        User user = repository.findOne(id);
        Resource<User> resource = new Resource<>(user);
        resource.add(entityLinks.linkToSingleResource(User.class, user.getId()));

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    HttpEntity delete(@PathVariable("id") String id) {
        repository.delete(id);

        return new ResponseEntity(HttpStatus.OK);
    }
}
