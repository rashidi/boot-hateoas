package my.zin.rashidi.hateoas;

import my.zin.rashidi.hateoas.domain.User;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BootHateoasApplication.class)
@WebAppConfiguration
@DirtiesContext
@IntegrationTest("server.port:0")
public class BootHateoasApplicationTests {

	@Value("${local.server.port}")
	private int port;

	private String url;
	private String userUrl;

	@Before
	public void save() {
		User user = new User("50m391883r15418", "rashidi@zin.my", "rashidi zin");

		url = String.format("http://localhost:%d/users", port);
		userUrl = url + "/" + user.getId();

		new TestRestTemplate().postForEntity(url, user, String.class);
	}

	@Test
	public void get() {
		ResponseEntity<String> entity = new TestRestTemplate().getForEntity(userUrl, String.class);

		assertEquals(HttpStatus.OK, entity.getStatusCode());
		assertThat(entity.getBody(), containsString("_links"));
	}

	@Test
	public void getAll() {
		ResponseEntity<String> entity = new TestRestTemplate().getForEntity(url, String.class);

		assertEquals(HttpStatus.OK, entity.getStatusCode());
		assertThat(entity.getBody(), containsString("users"));
	}

	@After
	public void delete() {
		new TestRestTemplate().delete(userUrl);
	}
}
